/* interact_TODO
 * Interact with deployed TODO contract
 * node interact_TODO.js {contractAddress}
 */
const fs = require('fs');
// directory where Web3 is stored, in Aion Kernel
global.Web3 = require('aion-web3');
// connecting to Aion local node
const web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));

// Importing unlock, compile and deploy scripts
const unlock = require('../contracts/unlock');
const compile = require('../contracts/compile');
const deploy = require('../contracts/deploy');
const readlineSync = require('readline-sync');


const sol = fs.readFileSync('../contracts/TODO.sol', {
    encoding: 'utf8'
});

let contractAddr = process.argv[2]; // Contract Address
let contractInstance;
let events;
let acc = web3.personal.listAccounts;
let a0 = acc[2];
let pw0 = "Dhruvin@1993";

Promise.all([
    // Unlock accounts & complile contract
    unlock(web3, a0, pw0),
    compile(web3, sol),
    console.log("\n[log] 1. unlocking account:", a0),
    console.log("[log] 2. compiling contract"),

]).then((res) => {
    let a0 = res[0];
    let abi = res[1].TODO.info.abiDefinition;
    let code = res[1].TODO.code;
    let content = process.argv[3];
    let author = process.argv[4]
    console.log("[log] accessing contract\n");
    console.log("balance = ", web3.eth.getBalance(a0));
    // Contract Instantiantion
    contractInstance = web3.eth.contract(abi).at(contractAddr);
    const hash = contractInstance.createTask(
        content,
        author,
        {
            from: a0,
            data: code
        }
    )
    console.log(`Hash ${hash}`);
    events = contractInstance.TaskCreated(
        {
            fromBlock: (web3.eth.blockNumber - 1),
            toBlock: 'latest'
        }
    )
    events.watch(function (err, res) {
        if ((res.transactionHash === hash) &&
            (res.event === 'TaskCreated') &&
            (res.args.content === content) &&
            (res.args.author === author)) {
            console.log('[log] Task Created! \n');
            console.log('Getting task Id...\n');
            let lastTaskID = contractInstance.getLastTaskId();
            console.log("Last task Id : ", lastTaskID.toString());
        }
        events.stopWatching();
    })

    // readlineSync.promptCLLoop({

    //     // create a task
    //     createTask: function (content, author) {
    //         const hash = contractInstance.createTask(
    //             content,
    //             author,
    //             {
    //                 from: a0,
    //                 data: code
    //             }
    //         )
    //         console.log('[log] Task Created! \n');
    //         console.log(`Hash ${hash}`);
    //     },
    //     //tx receipt
    //     receipt: function (hash) {
    //         // get receipt for given tx hash
    //         let txReceipt = web3.eth.getTransactionReceipt(hash);

    //         // print tx receipt
    //         console.log("transaction receipt:");
    //         console.log(txReceipt);
    //     },
    //     // get taskIDs
    //     getTaskIds: function () {
    //         let taskIDs = contractInstance.getTaskIds();
    //         console.log("Task Id : ", taskIDs);
    //     },

    //     // get lastTaskIDs
    //     getLastTaskId: function () {
    //         let lastTaskID = contractInstance.getLastTaskId();
    //         console.log("Last task Id : ", lastTaskID.toString());
    //     },

    //     getTask: function (index) {
    //         let task = contractInstance.getTask(index);
    //         console.log(`task ${task}`);
    //     },

    //     toggleTaskStatus: function (index) {
    //         let hash = contractInstance.toggleTaskStatus(
    //             index,
    //             {
    //                 from: a0,
    //                 date: code
    //             }
    //         );
    //         console.log('[log] task toggled');
    //         console.log(`hash ${hash}`);
    //     }
    // });
});
