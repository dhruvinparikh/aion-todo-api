import express from 'express';
import * as todo from './controllers/TODO'
import bodyParser from 'body-parser'
import cors from 'cors'
const app = express();

// use it before all route definitions
app.use(cors({origin: 'http://localhost:3000'}));
// to convert body request into json
app.use( bodyParser.json() );

// ---- CRUD API endpoints
app.post('/create', todo.create);
app.post('/toggle', todo.toggle);
app.get('/get/:id', todo.get);
app.post('/del', todo.del);
const port = process.env.PORT || 2500;
app.listen(port, () => console.log('Example app listening on port %d',port))