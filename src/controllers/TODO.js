'use strict';

const fs = require('fs');
// directory where Web3 is stored, in Aion Kernel
global.Web3 = require('aion-web3');
// connecting to Aion local node
const web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));

// Importing unlock, compile and deploy scripts
const unlock = require('../contracts/unlock');
const compile = require('../contracts/compile');
const readlineSync = require('readline-sync');


const sol = fs.readFileSync('../contracts/TODO.sol', {
    encoding: 'utf8'
});

let events;
let contractInstance;
let contractAddr = '0xa030680622f200d583ffc42d8890666f9924096b9f93bea456efcd438cb27449';
let a0 = '0xa0add6a30ea6441b135cdaec0cbf175ffded922e3f3f80925c311844dcaa4357';
let pw0 = "Dhruvin@1993";


function create(request, response) {
    let content = request.body.content;
    let author = request.body.author;
    Promise.all([
        // Unlock accounts & complile contract
        unlock(web3, a0, pw0),
        compile(web3, sol),
        console.log("\n[log] 1. unlocking account:", a0),
        console.log("[log] 2. compiling contract"),
    ]).then((res) => {
        let a0 = res[0];
        let abi = res[1].TODO.info.abiDefinition;
        let code = res[1].TODO.code;
        console.log("[log] accessing contract\n");
        // Contract Instantiation
        contractInstance = web3.eth.contract(abi).at(contractAddr);
        const hash = contractInstance.createTask(
            content,
            author,
            {
                from: a0,
                data: code
            }
        )
        events = contractInstance.TaskCreated(
            {
                fromBlock: (web3.eth.blockNumber - 1),
                toBlock: 'latest'
            }
        )
        events.watch(function (err, res) {
            if ((res.transactionHash === hash) &&
                (res.event === 'TaskCreated') &&
                (res.args.content === content) &&
                (res.args.author === author)) {
                console.log('[log] Task Created! \n');
                response.json(
                    {
                        'id': res.args.id,
                        'content': res.args.content,
                        'author': res.args.author,
                        'date': res.args.date,
                    }
                );
                events.stopWatching();
            }
            else {
                response.json({
                    'message': 'error ocurred'
                })
                events.stopWatching();
            }
        })
    });
}

function toggle(request, response) {
    let id = request.body.id;
    Promise.all([
        // Unlock accounts & complile contract
        unlock(web3, a0, pw0),
        compile(web3, sol),
        console.log("\n[log] 1. unlocking account:", a0),
        console.log("[log] 2. compiling contract"),

    ]).then((res) => {
        let a0 = res[0];
        let abi = res[1].TODO.info.abiDefinition;
        let code = res[1].TODO.code;
        console.log("[log] accessing contract\n");
        // Contract Instantiation
        contractInstance = web3.eth.contract(abi).at(contractAddr);
        const hash = contractInstance.toggleTaskStatus(
            id,
            {
                from: a0,
                data: code
            }
        )
        events = contractInstance.TaskStatusToggled(
            {
                fromBlock: (web3.eth.blockNumber - 1),
                toBlock: 'latest'
            }
        )
        events.watch(function (err, res) {
            console.log(`${JSON.stringify(res)}`);
            if ((res.transactionHash === hash) &&
                (res.event === 'TaskStatusToggled') &&
                (res.args.id == id)) {
                console.log('[log] Task status toggled! \n');
                response.json(
                    {
                        'id': res.args.id,
                        'status': res.args.done,
                    }
                );
                events.stopWatching();
            }
            else {
                response.json({
                    'message': 'error ocurred'
                })
                events.stopWatching();
            }
        })
    });
}

function get(request, response) {
    let id = parseInt(request.params.id);
    Promise.all([
        // Unlock accounts & complile contract
        unlock(web3, a0, pw0),
        compile(web3, sol),
        console.log("\n[log] 1. unlocking account:", a0),
        console.log("[log] 2. compiling contract"),

    ]).then((res) => {
        let a0 = res[0];
        let abi = res[1].TODO.info.abiDefinition;
        let code = res[1].TODO.code;
        console.log("[log] accessing contract\n");
        var task = [];
        contractInstance = web3.eth.contract(abi).at(contractAddr);
        let lastTaskId = ((id == 0) ? contractInstance.getLastTaskId().toString() : id);
        console.log('last task :', lastTaskId);
        for (var i = ((id === 0) ? 1 : id); i <= lastTaskId; i++) {
            const t = contractInstance.getTask(i)
            var newTask = {
                id: t[0].toString(),
                date: new Date(parseFloat(t[1].toString())),
                content: t[2],
                author: t[3],
                status: t[4]
            }
            console.log(t);
            task.push(newTask);
        }
        response.json({ task });
    });
}

function del(request, response) {
    let id = request.body.id;
    Promise.all([
        // Unlock accounts & complile contract
        unlock(web3, a0, pw0),
        compile(web3, sol),
        console.log("\n[log] 1. unlocking account:", a0),
        console.log("[log] 2. compiling contract"),

    ]).then((res) => {
        let a0 = res[0];
        let abi = res[1].TODO.info.abiDefinition;
        let code = res[1].TODO.code;
        console.log("[log] accessing contract\n");
        var task = [];
        contractInstance = web3.eth.contract(abi).at(contractAddr);
        const hash = contractInstance.removeTask(
            id,
            {
                from: a0,
                data: code
            }
        )
        events = contractInstance.TaskRemoved(
            {
                fromBlock: (web3.eth.blockNumber - 1),
                toBlock: 'latest'
            }
        )
        events.watch(function (err, res) {
            if ((res.transactionHash === hash) &&
                (res.event === 'TaskRemoved')) {
                console.log('[log] Task Removed! \n');
                response.json(
                    {
                        'count': res.args.lastTaskId,
                    }
                );
                events.stopWatching();
            }
            else {
                response.json({
                    'message': 'error ocurred'
                })
                events.stopWatching();
            }
        })
    });
}

export { create, toggle, get, del };