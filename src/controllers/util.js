'use strict';

const fs = require('fs');
// directory where Web3 is stored, in Aion Kernel
global.Web3 = require('aion-web3');
// connecting to Aion local node
const web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));

// Importing unlock, compile and deploy scripts
const unlock = require('../contracts/unlock');
const compile = require('../contracts/compile');

let abi;
let code;
let contractAddress = '0xa0b0ec29f3363028327a694764fdc0bb9a33ca6fa92678fa96d0635146683549';
let a0 = '0xa0add6a30ea6441b135cdaec0cbf175ffded922e3f3f80925c311844dcaa4357';
let pw0 = 'Dhruvin@1993';

const sol = fs.readFileSync('../contracts/TODO.sol', {
    encoding: 'utf8'
});

function getUtil() {
    return Promise.all([
        // Unlock accounts & complile contract
        unlock(web3, a0, pw0),
        compile(web3, sol),
        console.log('\n[log] 1. unlocking account: ', a0),
        console.log('[log] 2. compiling contract'),
    ]).then((res) => {
        console.log(`${res}`);
        a0 = res[0];
        abi = res[1].TODO.info.abiDefinition;
        code = res[1].TODO.code;
        console.log('[log] 3. Accessing contract');
    });
}
export { getUtil };
